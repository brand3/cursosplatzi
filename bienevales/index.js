var view = document.getElementById("full-form");
const inicarSesionTexto = "Enterate de los beneficios"
const registrateTexto = "Registrate para obtenerlos"
const linkTextoInicio = "Inicio"
const linkTextoIniciarSesion = "Iniciar sesión"
const campoRequeridoTexto = "Este campo es requerido"
const textoCorreoInvalido = "Correo electronico invalido. Ejemplo: ejemplo@email.com"
const textoTelefonoInvalido = "Numero de celular invalido. Ejemplo: 3333333333"
const textoPasswordInvalido = "La contraseña debe tener al menos 8 caracteres"
const textoVerificaPassword = "Las contraseñas deben ser iguales"
/**
 * Llama a las funciones especificas para cambiar vistas
 * @param {string} estado 
 */
let changeView = (estado) => {
    view.classList.add("hide")

    if(estado == "Inicio"){
        inicio();
    }
    else if(estado == "Iniciar sesión"){
        iniciarSesion();
    }
    else if(estado === "contrasenia"){
        recuperarContrasenia();
    }
    else if(estado === "registro"){
        registrateAhora();
    }
    view.classList.remove("hide")
}


let iniciarSesion = () => {
    view = document.getElementById("full-form");
    changeText(inicarSesionTexto, linkTextoInicio)
}

let inicio = () => {
    view = document.getElementById("verificacion");
    changeText(inicarSesionTexto, linkTextoIniciarSesion )
}

let recuperarContrasenia = ()=> {
    view = document.getElementById("recuperar-contrasenia");
    changeText(inicarSesionTexto, linkTextoIniciarSesion)
}

let registrateAhora = () =>{
    view = document.getElementById("registro");
    changeText(registrateTexto, linkTextoIniciarSesion)
}

let changeText = (texto = "", linkTexto) =>{
    let textoYellowBanner = document.getElementById('yellow-banner-text');
    let link = document.getElementById('linkNavigator');

    if(texto !== ""){
        textoYellowBanner.innerHTML = texto;
    }
    link.innerHTML = linkTexto;
}


let validateEmail = (emailInput) =>{
    let emailLabel = emailInput.parentElement;
    let email = emailInput.value;
    const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if(!re.test(email))
    {
        if(emailLabel.childElementCount < 3){
            agregarError(emailLabel, textoCorreoInvalido)
        }
        else {
            changeTextError(emailLabel.lastElementChild, textoCorreoInvalido);
        }
    }
    else if(emailLabel.childElementCount === 3 && email.length > 0) {
        emailLabel.removeChild(emailLabel.lastElementChild);
    }
    if( email.length === 0){
        emailLabel.removeChild(emailLabel.lastElementChild);
        agregarError(emailLabel,campoRequeridoTexto)
    }
}

let validatePhoneNumber = (number) => {
    let numberLabel = document.getElementById("celularLabel");
    const re = /^[0-9]+$/;
    if(number.length < 10)
    {
        //delete value
        if(!re.test(number)){
            input = numberLabel.childNodes;
            input[3].value = input[3].value.slice(0,-1)
        }
       
        if(numberLabel.childElementCount < 3){
            agregarError(numberLabel, textoTelefonoInvalido)
        }
        else{
            changeTextError(numberLabel.lastElementChild, textoTelefonoInvalido);

        }
    }
    else if(numberLabel.childElementCount === 3) {
        numberLabel.removeChild(numberLabel.lastElementChild);
    }
    if(number.length === 0){
        numberLabel.removeChild(numberLabel.lastElementChild);
        agregarError(numberLabel,campoRequeridoTexto)
    }
}

let validatePassword = (passInput) =>{
    let passLabel = passInput.parentElement;
    let pass = passInput.value;
    if(pass.length < 8)
    {
        if(passLabel.childElementCount < 3){
            agregarError(passLabel, textoPasswordInvalido)
        }
        else{
            changeTextError(passLabel.lastElementChild, textoPasswordInvalido);
        }
    }
    else if(passLabel.childElementCount === 3) {
        passLabel.removeChild(passLabel.lastElementChild);
    }
    if(pass.length === 0){
        passLabel.removeChild(passLabel.lastElementChild);
        agregarError(passLabel,campoRequeridoTexto)
    }
}

let validateConfirmPassword = (pass) =>{
    let confirmPassLabel = document.getElementById("confirmPassLabel");
    let password = document.getElementById("passLabel").childNodes;
    if(pass !== password[3].value)
    {
        if(confirmPassLabel.childElementCount < 3){
            agregarError(confirmPassLabel, textoVerificaPassword)
        }
        else {
            changeTextError(confirmPassLabel.lastElementChild, textoVerificaPassword);
        }
    }
    else if(confirmPassLabel.childElementCount === 3 && pass.length > 0) {
        confirmPassLabel.removeChild(confirmPassLabel.lastElementChild);
    }
    else if(pass.length === 0){
        confirmPassLabel.removeChild(confirmPassLabel.lastElementChild);
        agregarError(confirmPassLabel,campoRequeridoTexto)
    }
}

const requerido = (input) => {
    if(input.type === "checkbox"){
        if( (!input.checked) && input.parentElement.childElementCount < 3){
            agregarError(input.parentElement, campoRequeridoTexto)
        }
        else if(input.parentElement.childElementCount === 3){
            input.parentElement.removeChild(input.parentElement.lastElementChild);
        }
    }
    else {
        if(input.value.length === 0 && input.parentElement.childElementCount < 3){
            agregarError(input.parentElement, campoRequeridoTexto)
        }
        else if(input.parentElement.childElementCount === 3){
            input.parentElement.removeChild(input.parentElement.lastElementChild);
        }
    }
}

let agregarError = (nodeAppendingTo, textError) =>{
    let error = document.createElement("div"); 
    error.className = "error";

    error.innerHTML = textError;
    nodeAppendingTo.appendChild(error)
}

function changeTextError(error,texto){
    error.innerHTML = texto;
}


const submitForm = (btn) =>{
    let form = btn.parentElement;
    nodeList = form.elements;
    let pass1;
    let errorFlag = false;

    for(let i = 0; i < nodeList.length;i++){
       
        if( (nodeList[i].value.length === 0 || (nodeList[i].type === "checkbox" && !nodeList[i].checked))
            && nodeList[i] instanceof HTMLInputElement
            && nodeList[i].parentElement.childElementCount < 3){

            if(nodeList[i].parentElement.childElementCount < 3) {
                agregarError(nodeList[i].parentElement,"Este campo es requerido")
                errorFlag = true;
            }
        }
        if(nodeList[i].type === "password"){
            if(pass1 !== undefined && pass1 !== nodeList[i].value){
                agregarError(nodeList[i].parentElement,"Las contraseñas deben ser iguales")
                errorFlag = true;
            }
            pass1 = nodeList[i].value;
        }
        if(nodeList[i].oninput === null 
            && (nodeList[i].type === 'email' || nodeList[i].type === "password")){
               if(nodeList[i].type === 'email'){
                    nodeList[i].setAttribute("oninput","validateEmail(this);");
                    validateEmail(nodeList[i]);
               }
               else{
                    nodeList[i].setAttribute("oninput","validatePassword(this);");
                    validatePassword(nodeList[i]);
               }
           } 
    }
    if(!errorFlag){
        form.checkValidity();
        return true;
    }
    return false;
}

