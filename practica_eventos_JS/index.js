const eventos = [];
Object.keys(window).forEach((key) => {
  if (/^on/.test(key)) {
    eventos.push(key.slice(2));
  }
});

addAllEventListenersAvailable = (element, valorElement, eventoElement) => {
  eventos.forEach((evento) => {
    element.addEventListener(evento, (event) => {
      if (element.type === "checkbox" || element.type === "radio") {
        valorElement.innerHTML = element.checked;
      } else {
        valorElement.innerHTML = element.value;
      }
      eventoElement.innerHTML = event.type;
    });
  });
};

addEventListenerToAllEntities = (jsonLoad = null) => {
  let filas = document.getElementsByTagName("tr");
  let jsonLoadIndex1 = 3
  let jsonLoadIndex2 = 5
  let jsonLoadIndex3 = 1
  let start = 1;
  if(jsonLoad){
    jsonLoadIndex1 = 1
    jsonLoadIndex2 = 2
    jsonLoadIndex3 = 0
    start = 23
  }
  for (let i = start; i < filas.length; i++) {
    let input = filas[i].childNodes[jsonLoadIndex3].childNodes[jsonLoadIndex3];
    let valorElement = filas[i].childNodes[jsonLoadIndex1].childNodes[jsonLoadIndex3];
    let eventoElement = filas[i].childNodes[jsonLoadIndex2].childNodes[jsonLoadIndex3];
    addAllEventListenersAvailable(input, valorElement, eventoElement);
  }
};

renderHTML = () => {
  document.getElementsByTagName("table")[0].classList.remove("hide");
  addEventListenerToAllEntities();

};
renderJson = () => {
  iterateOverJson(json2, "body");
  addEventListenerToAllEntities(true);
};

createHTML = (element, father = "body", value = null, type = null) => {
  if (father === "body") {
    if (value) {
      let fatherElement = document.getElementsByName(father);
      let htmlElement = document.createElement(element);
      let paragraph = document.createElement("p");
      paragraph.innerHTML = value;
      htmlElement.appendChild(paragraph);
      fatherElement.appendChild(htmlElement);
      return htmlElement;
      
    }
    else {
     let fatherElement = document.getElementsByTagName(father)[0];
      let htmlElement = document.createElement(element);
      fatherElement.appendChild(htmlElement);
      return htmlElement;
    }
  }
  else {
    if (!value) {
      let fatherElement = father;
      let htmlElement = document.createElement(element);
      if(type){
        htmlElement.setAttribute('type', type);
      }
      fatherElement.appendChild(htmlElement);
      return htmlElement;
    }
    else {
      let fatherElement = father;
      let htmlElement = document.createElement(element);
      let paragraph = document.createElement("p");
      paragraph.innerHTML = value;
      htmlElement.appendChild(paragraph);
      fatherElement.appendChild(htmlElement);
      return htmlElement;
    }
  }
};

iterateOverJson = (value, father) => {
  for (let i = 0; i < Object.getOwnPropertyNames(value).length; i++) {
    let elementName = Object.getOwnPropertyNames(value)[i];
    let element = value[elementName];
    let dad = undefined

    if(Array.isArray(element)){
      dad = createHTML(elementName, father);
      for(let y = 0; y < element.length; y++){
        iterateOverJson(element[y], dad)
      }
      i =  Object.getOwnPropertyNames(value).length
    }

    if (!(typeof element === "object" && element !== null)) {
      createHTML(elementName, father, element);
      return;
    }
    else {
      if (elementName === "input") {
        createHTML(elementName,father,undefined,element.type)
        return;
      }
      if(dad === undefined){
        dad = createHTML(elementName, father);
      }
      if(!Array.isArray(element)){
        iterateOverJson(element, dad);
      }
    }
  }
};

let json2 = {
  table: {
    thead: {
      tr:
        [
          { th: "input" },
          { th: "valor" },
          { th: "evento" },
        ],
    },
    tbody: [
      {
        tr: [{
          td: {
            input: {
              type: "text",
            },
          }
        },
        {
          td: {
            p: "",
          }
        },
        {
          td: {
            p: "",
          },
        }]
      },
      {
        tr: [
          {
            td: {
              input: {
                type: "button",
              },
            }
          },
          {
            td: {
              p: "",
            }
          },
          {
            td: {
              p: "",
            },
          }]
      },
      {
        tr: [
          {
            td: {
              input: {
                type: "checkbox",
              },
            }
          },
          {
            td: {
              p: "",
            }
          },
          {
            td: {
              p: "",
            },
          }]
      },
      {
        tr: [
          {
            td: {
              input: {
                type: "color",
              },
            }
          },
          {
            td: {
              p: "",
            }
          },
          {
            td: {
              p: "",
            },
          }]
      },
      {
        tr: [
          {
            td: {
              input: {
                type: "date",
              },
            }
          },
          {
            td: {
              p: "",
            }
          },
          {
            td: {
              p: "",
            },
          }]
      },
      {
        tr: [
          {
            td: {
              input: {
                type: "datetime",
              },
            }
          },
          {
            td: {
              p: "",
            }
          },
          {
            td: {
              p: "",
            },
          }]
      },
      {
        tr: [
          {
            td: {
              input: {
                type: "datetime-local",
              },
            }
          },
          {
            td: {
              p: "",
            }
          },
          {
            td: {
              p: "",
            },
          }]
      },
      {
        tr: [
          {
            td: {
              input: {
                type: "email",
              },
            }
          },
          {
            td: {
              p: "",
            }
          },
          {
            td: {
              p: "",
            },
          }]
      },
      {
        tr: [
          {
            td: {
              input: {
                type: "image",
              },
            }
          },
          {
            td: {
              p: "",
            }
          },
          {
            td: {
              p: "",
            },
          }]
      },
      {
        tr: [
          {
            td: {
              input: {
                type: "month",
              },
            }
          },
          {
            td: {
              p: "",
            }
          },
          {
            td: {
              p: "",
            },
          }]
      },
      {
        tr: [
          {
            td: {
              input: {
                type: "number",
              },
            }
          },
          {
            td: {
              p: "",
            }
          },
          {
            td: {
              p: "",
            },
          }]
      },
      {
        tr: [
          {
            td: {
              input: {
                type: "radio",
              },
            }
          },
          {
            td: {
              p: "",
            }
          },
          {
            td: {
              p: "",
            },
          }]
      },
      {
        tr: [
          {
            td: {
              input: {
                type: "range",
              },
            }
          },
          {
            td: {
              p: "",
            }
          },
          {
            td: {
              p: "",
            },
          }]
      },
      {
        tr: [
          {
            td: {
              input: {
                type: "reset",
              },
            }
          },
          {
            td: {
              p: "",
            }
          },
          {
            td: {
              p: "",
            },
          }]
      },
      {
        tr: [
          {
            td: {
              input: {
                type: "search",
              },
            }
          },
          {
            td: {
              p: "",
            }
          },
          {
            td: {
              p: "",
            },
          }]
      },
      {
        tr: [
          {
            td: {
              input: {
                type: "submit",
              },
            }
          },
          {
            td: {
              p: "",
            }
          },
          {
            td: {
              p: "",
            },
          }]
      },
      {
        tr: [
          {
            td: {
              input: {
                type: "tel",
              },
            }
          },
          {
            td: {
              p: "",
            }
          },
          {
            td: {
              p: "",
            },
          }]
      },
      {
        tr: [
          {
            td: {
              input: {
                type: "time",
              },
            }
          },
          {
            td: {
              p: "",
            }
          },
          {
            td: {
              p: "",
            },
          }]
      },
      {
        tr: [
          {
            td: {
              input: {
                type: "url",
              },
            }
          },
          {
            td: {
              p: "",
            }
          },
          {
            td: {
              p: "",
            },
          }]
      },
      {
        tr: [
          {
            td: {
              input: {
                type: "week",
              },
            }
          },
          {
            td: {
              p: "",
            }
          },
          {
            td: {
              p: "",
            },
          }
        ]
      },
    ],
  },
};
